This repository provides supporting materials for the following paper

Islam, M. K., Aridhi, S., Smail-Tabbone, M., Simple negative sampling for link prediction inknowledge graphs, The 10th International Conference on Complex Networks and their Applications, Madrid, Spain, 2021 (Accepted).

